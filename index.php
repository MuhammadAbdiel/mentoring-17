<?php

require_once __DIR__ . '/vendor/autoload.php';

\Sentry\init(['dsn' => 'https://glet_4e8bd6f26066c83cc6e6aee04640b235@observe.gitlab.com:443/errortracking/api/v1/projects/51528006']);

try {
  throw new \Exception('First error');
} catch (\Throwable $exception) {
  \Sentry\captureException($exception);
}
